import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {User, UserWrapper} from "../user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private API: string = 'https://gorest.co.in/public/v1/users';

  constructor(private http: HttpClient) { }

  getUser(): Observable<UserWrapper>{
    return this.http.get<UserWrapper>(this.API);
  }

  // Error handling
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
