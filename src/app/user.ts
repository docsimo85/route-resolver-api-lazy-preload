export interface User {
  id: number,
  name: string,
  email: string,
  gender: string,
  status: string
}

export interface UserWrapper {
  meta: any,
  data: User[]
}

