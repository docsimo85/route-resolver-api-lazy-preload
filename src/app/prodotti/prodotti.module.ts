import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProdottiRoutingModule } from './prodotti-routing.module';
import { ProdottiComponent } from './prodotti.component';


@NgModule({
  declarations: [
    ProdottiComponent
  ],
  imports: [
    CommonModule,
    ProdottiRoutingModule
  ]
})
export class ProdottiModule { }
