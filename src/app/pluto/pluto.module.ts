import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlutoRoutingModule } from './pluto-routing.module';
import { PlutoComponent } from './pluto.component';


@NgModule({
  declarations: [
    PlutoComponent
  ],
  imports: [
    CommonModule,
    PlutoRoutingModule
  ]
})
export class PlutoModule { }
