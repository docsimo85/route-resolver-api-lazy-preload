import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlutoComponent } from './pluto.component';

const routes: Routes = [{ path: '', component: PlutoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlutoRoutingModule { }
