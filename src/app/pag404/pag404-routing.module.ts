import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Pag404Component } from './pag404.component';

const routes: Routes = [{ path: '', component: Pag404Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Pag404RoutingModule { }
