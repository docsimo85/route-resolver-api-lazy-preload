import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Pag404RoutingModule } from './pag404-routing.module';
import { Pag404Component } from './pag404.component';


@NgModule({
  declarations: [
    Pag404Component
  ],
  imports: [
    CommonModule,
    Pag404RoutingModule
  ]
})
export class Pag404Module { }
