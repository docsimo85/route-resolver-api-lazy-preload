import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PippoComponent } from './pippo.component';

const routes: Routes = [{ path: '', component: PippoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PippoRoutingModule { }
