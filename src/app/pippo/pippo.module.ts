import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PippoRoutingModule } from './pippo-routing.module';
import { PippoComponent } from './pippo.component';


@NgModule({
  declarations: [
    PippoComponent
  ],
  imports: [
    CommonModule,
    PippoRoutingModule
  ]
})
export class PippoModule { }
