import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: 'pippo',
    loadChildren: () => import('./pippo/pippo.module').then(m => m.PippoModule)
  },
  {
    path: 'pluto',
    loadChildren: () => import('./pluto/pluto.module').then(m => m.PlutoModule)
  },
  {
    path: 'prodotti',
    loadChildren: () => import('./prodotti/prodotti.module').then(m => m.ProdottiModule)
  },
  {
    path: 'servizi',
    loadChildren: () => import('./servizi/servizi.module').then(m => m.ServiziModule)
  },
  {
    path: 'contatti',
    loadChildren: () => import('./contatti/contatti.module').then(m => m.ContattiModule)
  },
  {path: '404', loadChildren: () => import('./pag404/pag404.module').then(m => m.Pag404Module)},
  { path: 'user', loadChildren: () => import('./user/user.module').then(m => m.UserModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
      // preloadingStrategy: PreloadAllModules
    })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
