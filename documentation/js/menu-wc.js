'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">lazy documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-d576d324fffc25ecb9ba457fd754a58c"' : 'data-target="#xs-components-links-module-AppModule-d576d324fffc25ecb9ba457fd754a58c"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-d576d324fffc25ecb9ba457fd754a58c"' :
                                            'id="xs-components-links-module-AppModule-d576d324fffc25ecb9ba457fd754a58c"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ContattiModule.html" data-type="entity-link" >ContattiModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ContattiModule-5d1737d40ed13f96070fb403dba0704e"' : 'data-target="#xs-components-links-module-ContattiModule-5d1737d40ed13f96070fb403dba0704e"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ContattiModule-5d1737d40ed13f96070fb403dba0704e"' :
                                            'id="xs-components-links-module-ContattiModule-5d1737d40ed13f96070fb403dba0704e"' }>
                                            <li class="link">
                                                <a href="components/ContattiComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ContattiComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ContattiRoutingModule.html" data-type="entity-link" >ContattiRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/Pag404Module.html" data-type="entity-link" >Pag404Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Pag404Module-285b6b4fdc06b0449432e3e4a3b94699"' : 'data-target="#xs-components-links-module-Pag404Module-285b6b4fdc06b0449432e3e4a3b94699"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Pag404Module-285b6b4fdc06b0449432e3e4a3b94699"' :
                                            'id="xs-components-links-module-Pag404Module-285b6b4fdc06b0449432e3e4a3b94699"' }>
                                            <li class="link">
                                                <a href="components/Pag404Component.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >Pag404Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Pag404RoutingModule.html" data-type="entity-link" >Pag404RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PippoModule.html" data-type="entity-link" >PippoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PippoModule-dd23995d8eb744062f49bf2b21d91557"' : 'data-target="#xs-components-links-module-PippoModule-dd23995d8eb744062f49bf2b21d91557"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PippoModule-dd23995d8eb744062f49bf2b21d91557"' :
                                            'id="xs-components-links-module-PippoModule-dd23995d8eb744062f49bf2b21d91557"' }>
                                            <li class="link">
                                                <a href="components/PippoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PippoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PippoRoutingModule.html" data-type="entity-link" >PippoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/PlutoModule.html" data-type="entity-link" >PlutoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-PlutoModule-4d13bf406e5eabc4b3a324e7a94474a5"' : 'data-target="#xs-components-links-module-PlutoModule-4d13bf406e5eabc4b3a324e7a94474a5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-PlutoModule-4d13bf406e5eabc4b3a324e7a94474a5"' :
                                            'id="xs-components-links-module-PlutoModule-4d13bf406e5eabc4b3a324e7a94474a5"' }>
                                            <li class="link">
                                                <a href="components/PlutoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PlutoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/PlutoRoutingModule.html" data-type="entity-link" >PlutoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ProdottiModule.html" data-type="entity-link" >ProdottiModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ProdottiModule-0750ab1c06c1625e6a165d7341692762"' : 'data-target="#xs-components-links-module-ProdottiModule-0750ab1c06c1625e6a165d7341692762"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProdottiModule-0750ab1c06c1625e6a165d7341692762"' :
                                            'id="xs-components-links-module-ProdottiModule-0750ab1c06c1625e6a165d7341692762"' }>
                                            <li class="link">
                                                <a href="components/ProdottiComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ProdottiComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProdottiRoutingModule.html" data-type="entity-link" >ProdottiRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ServiziModule.html" data-type="entity-link" >ServiziModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ServiziModule-ded9a84b89a9bfbd00bfb662ecc3f504"' : 'data-target="#xs-components-links-module-ServiziModule-ded9a84b89a9bfbd00bfb662ecc3f504"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ServiziModule-ded9a84b89a9bfbd00bfb662ecc3f504"' :
                                            'id="xs-components-links-module-ServiziModule-ded9a84b89a9bfbd00bfb662ecc3f504"' }>
                                            <li class="link">
                                                <a href="components/ServiziComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ServiziComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ServiziRoutingModule.html" data-type="entity-link" >ServiziRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UserModule-29dfeb2829e291695205a0563ac2739f"' : 'data-target="#xs-components-links-module-UserModule-29dfeb2829e291695205a0563ac2739f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UserModule-29dfeb2829e291695205a0563ac2739f"' :
                                            'id="xs-components-links-module-UserModule-29dfeb2829e291695205a0563ac2739f"' }>
                                            <li class="link">
                                                <a href="components/UserComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UserRoutingModule.html" data-type="entity-link" >UserRoutingModule</a>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link" >UserService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/UserResolverService.html" data-type="entity-link" >UserResolverService</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/User.html" data-type="entity-link" >User</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/UserWrapper.html" data-type="entity-link" >UserWrapper</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});